﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

//объявим 3 коллеции ипеременную для счетчика времени
List<int> listInt = new ();
ArrayList listVar = new  ();
LinkedList<int> listLinked = new ();
Stopwatch workTime = new Stopwatch ();

//заполняем коллеции и выводим время их заполнения
workTime.Start();
for (var i = 0; i < 1000000; i++)
{
    listInt.Add(i);  
}
workTime.Stop();

Console.WriteLine($"Время заполнения списка listInt {workTime.ElapsedMilliseconds} мс");

workTime.Restart();
for (var i = 0; i < 1000000; i++)
{
    listVar.Add(i);
}
workTime.Stop();

Console.WriteLine($"Время заполнения списка listVar {workTime.ElapsedMilliseconds} мс");

workTime.Restart();
for (var i = 0; i < 1000000; i++)
{
    listLinked.AddFirst(i);
}
workTime.Stop();

Console.WriteLine($"Время заполнения списка listLinked {workTime.ElapsedMilliseconds} мс");

//ищем время поиска 496753 элемента коллекции
workTime.Restart();
int forInt = listInt[496753];
workTime.Stop();
Console.WriteLine($"Время поиска 496753 элемента списка listInt {workTime.ElapsedMilliseconds} мс");

workTime.Restart();
forInt = (int)listVar[496753];
workTime.Stop();
Console.WriteLine($"Время поиска 496753 элемента списка listVar {workTime.ElapsedMilliseconds} мс");

workTime.Restart();
int counter = 0;
foreach(int a in listLinked)
{
    if (counter< 496753)
        counter++;
    else
    {
        forInt = a; break;
    }
}
workTime.Stop();
Console.WriteLine($"Время поиска 496753 элемента списка listLinked {workTime.ElapsedMilliseconds} мс");

// listInt делим на 777
workTime.Restart();

Find777(listInt, "listInt");
workTime.Stop();
Console.WriteLine($"Время поиска элементов списка listLinked, которые делятся на 777 без остатка {workTime.ElapsedMilliseconds} мс");
// listVar делим на 777
workTime.Restart();

Find777(listVar, "listVar");
workTime.Stop();
Console.WriteLine($"Время поиска элементов списка listVar, которые делятся на 777 без остатка {workTime.ElapsedMilliseconds} мс");

// listLinked делим на 777
workTime.Restart();

Find777(listLinked, "listLinked");
workTime.Stop();
//метод для поиска элементов коллекции, которые делятся на 777 без остатка
static void Find777(ICollection collection, string nameList)
{
    Console.Write($"Элементы, которые делятся на 777 в коллекции {nameList}: ");
    foreach (var element in collection)
    {
        if ((int)element % 777 == 0)
            Console.Write($"{element} ");
    }
    Console.WriteLine();
}